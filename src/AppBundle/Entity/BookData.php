<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BookData
 *
 * @ORM\Table(name="book_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BookDataRepository")
 */
class BookData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="age_rate", type="smallint", nullable=true)
     */
    private $ageRate;

    /**
     * @var int
     *
     * @ORM\Column(name="page_count", type="smallint", nullable=true)
     */
    private $pageCount;

    /**
     * @var int
     *
     * @ORM\Column(name="book_id", type="integer", unique=true)
     */
    private $bookId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ageRate
     *
     * @param integer $ageRate
     *
     * @return BookData
     */
    public function setAgeRate($ageRate)
    {
        $this->ageRate = $ageRate;

        return $this;
    }

    /**
     * Get ageRate
     *
     * @return int
     */
    public function getAgeRate()
    {
        return $this->ageRate;
    }

    /**
     * Set pageCount
     *
     * @param integer $pageCount
     *
     * @return BookData
     */
    public function setPageCount($pageCount)
    {
        $this->pageCount = $pageCount;

        return $this;
    }

    /**
     * Get pageCount
     *
     * @return int
     */
    public function getPageCount()
    {
        return $this->pageCount;
    }

    /**
     * Set bookId
     *
     * @param integer $bookId
     *
     * @return BookData
     */
    public function setBookId($bookId)
    {
        $this->bookId = $bookId;

        return $this;
    }

    /**
     * Get bookId
     *
     * @return int
     */
    public function getBookId()
    {
        return $this->bookId;
    }
}

