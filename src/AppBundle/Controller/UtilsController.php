<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\{BookData, Library};;
use Symfony\Component\HttpFoundation\JsonResponse;

class UtilsController extends Controller
{
    /**
     * @Route("/list", name="list")
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $completeList = $em->getRepository('AppBundle:Library')->findAll();
        
 
        return $this->render('application/list.html.twig',
                array(
                    'list' => $completeList
                )
            );
    }
    
    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('AppBundle:Library')->find($id);
        $bookData = $em->getRepository('AppBundle:BookData')->findOneBy(array('bookId'=>$id));
 
        return $this->render('application/detail.html.twig',
                array(
                    'book' => $book,
                    'bookInfo' => $bookData,
                    'segment' => 'edit'
                )
            );
    }
    
    /**
     * @Route("/view/{id}", name="view")
     */
    public function viewAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('AppBundle:Library')->find($id);
        if($book){
            $bookData = $em->getRepository('AppBundle:BookData')->findOneBy(array('bookId'=>$id));
 
            return $this->render('application/detail.html.twig',
                    array(
                        'book' => $book,
                        'bookInfo' => $bookData,
                        'segment' => 'view'
                    )
                );
        }
        return $this->render('application/detail.html.twig',
                    array(
                        'book' => '',
                        'bookInfo' => '',
                        'segment' => 'view'
                    )
                );
    }

    /**
     * @Route("/persist", name="persist")
     */
    public function persistAction(Request $request)
    {
        $id = $_POST['id'];
        $title = $_POST['title'];
        $author = $_POST['author'];
        $year = $_POST['year'];
        $isbn = $_POST['isbn'];
        $ageRate = $_POST['ageRate'];
        $pageCount = $_POST['pageCount'];
        
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('AppBundle:Library')->find($id);
        if($book){
            $bookData = $em->getRepository('AppBundle:BookData')->findOneBy(array('bookId'=>$id));
            $book->setAuthor($author);
            $book->setTitle($title);
            $book->setYear($year);
            $book->setIsbn($isbn);
            $bookData->setAgeRate($ageRate);
            $bookData->setPageCount($pageCount);
            $em->persist($book);
            $em->persist($bookData);
            $em->flush();  
        }
        $response = new JsonResponse(array(
            'resp' => 'ok'));
        return $response;

    }
    
    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('AppBundle:Library')->find($id);
        if($book){
            $bookData = $em->getRepository('AppBundle:BookData')->findOneBy(array('bookId'=>$id));
            $em->remove($book);
            $em->remove($bookData);
            $em->flush();  
        }
        return $this->redirectToRoute('list');
    }
    
    /**
     * @Route("/add", name="add")
     */
    public function addAction(Request $request)
    {
        return $this->render('application/add.html.twig',
                    array(
                        'newBook'=>null,
                        'newBookData'=>null,
                        'error'=>null
                    ));
    }
    
    /**
     * @Route("/add/verify", name="addVerify")
     */
    public function addPersistAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $bookAlreadyExist = $em->getRepository('AppBundle:Library')->findBy(
                array(
                    'isbn'=>$request->request->get('isbn')
                )
            );
        $newBook = new Library();
        $newBookData = new BookData();
        if($bookAlreadyExist){
            $newBook->setAuthor($request->request->get('author'));
            $newBook->setIsbn($request->request->get('isbn'));
            $newBook->setTitle($request->request->get('title'));
            $newBook->setYear($request->request->get('year'));
            $newBookData->setBookId($newBook->getId());
            $newBookData->setAgeRate($request->request->get('ageRate'));
            $newBookData->setPageCount($request->request->get('pageCount'));

            return $this->errorAction($request,$newBook,$newBookData);
            
        }else{
            $newBook->setAuthor($request->request->get('author'));
            $newBook->setIsbn($request->request->get('isbn'));
            $newBook->setTitle($request->request->get('title'));
            $newBook->setYear($request->request->get('year'));
            $em->persist($newBook);
            $em->flush();
            $newBookData->setBookId($newBook->getId());
            $newBookData->setAgeRate($request->request->get('ageRate'));
            $newBookData->setPageCount($request->request->get('pageCount'));
            $em->persist($newBookData);
            $em->flush();
            return $this->redirectToRoute('add');
        }
        
    }
    
    private function errorAction(Request $request, Library $newBook, BookData $newBookData)
    {

        return $this->render('application/add.html.twig',
                    array(
                        'newBook'=>$newBook,
                        'newBookData'=>$newBookData,
                        'error'=>'true'
                    )
                );
    }
    
}
