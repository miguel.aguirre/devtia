Repositorio creado para la prueba técnica de Devtia

Para la instalación de este aplicativo se deberá ejecutar la siguiente lista de comandos tras la clonación del presente repositorio:

composer install
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force 

Una vez completados estos pasos, se deberá ejecutar el script que inserta unos datos con los que será posible hacer pruebas 
(el último paso no es necesario si se ejecuta este script, pero es recomendable ejecutarlo de todos modos por si no se llegase a lanzar):

-- ******************************* INICIO DEL SCRIPT ***************************************

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla devtia.book_data
DROP TABLE IF EXISTS `book_data`;
CREATE TABLE IF NOT EXISTS `book_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age_rate` smallint(6) DEFAULT NULL,
  `page_count` smallint(6) DEFAULT NULL,
  `book_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B46EB86B16A2B381` (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla devtia.book_data: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `book_data` DISABLE KEYS */;
INSERT INTO `book_data` (`id`, `age_rate`, `page_count`, `book_id`) VALUES
	(1, 18, 621, 1),
	(2, 0, 615, 2),
	(3, 18, 425, 3),
	(4, 18, 715, 4),
	(5, 0, 325, 5),
	(6, 0, 654, 6),
	(7, 0, 512, 7),
	(8, 12, 352, 8),
	(9, 18, 125, 9),
	(10, 0, 524, 10),
	(11, 12, 563, 11),
	(12, 0, 256, 12),
	(13, 18, 236, 13),
	(14, 18, 265, 14),
	(15, 12, 354, 15);
/*!40000 ALTER TABLE `book_data` ENABLE KEYS */;

-- Volcando estructura para tabla devtia.library
DROP TABLE IF EXISTS `library`;
CREATE TABLE IF NOT EXISTS `library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `year` smallint(6) NOT NULL,
  `isbn` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A18098BCCC1CF4E6` (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla devtia.library: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `library` DISABLE KEYS */;
INSERT INTO `library` (`id`, `title`, `author`, `year`, `isbn`) VALUES
	(1, 'El tránsito terreno', 'Plasencia, Juan Luis', 1996, '84-121-2310-1'),
	(2, 'Sistemas operativos', 'Bazilian Eric', 1994, '84-7489-146-9'),
	(3, 'Poemas intrínsecos', 'Llorens Antonia', 1997, '84-305-0473-7'),
	(4, 'Avances en Arquitectura', 'Richter, Helmut', 1991, '84-473-0120-6'),
	(5, 'Las balas del bien', 'Leverling, Janet', 1995, '84-206-1704-0'),
	(6, 'La mente y el sentir', 'Plasencia, Juan Luis', 1992, '84-226-2128-2'),
	(7, 'Ensayos póstumos', 'Bertomeu, Andrés', 1995, '84-7908-349-2'),
	(8, 'La dualidad aparente', 'Sanabria, Carmelo', 1994, '84-578-0214-8'),
	(9, 'Arquitectura y arte', 'Richter, Helmut', 1992, '84-02-08696-9'),
	(10, 'Historia de Occidente', 'Dulac, George', 1995, '84-01-92101-5'),
	(11, 'Sentimiento popular', 'Llorens, Antonia', 1994, '84-7634-421-1'),
	(12, 'Amigos o enemigos', 'Sanabria, Carmelo', 1996, '84-404-8586-7'),
	(13, 'La burguesía del XIX', 'Dulac, George', 1996, '84-205-1101-3'),
	(14, 'Procesadores cuánticos', 'Bazilian, Eric', 1997, '84-212-2121-2'),
	(15, 'Canto de esperanza', 'Davolio, Nancy', 1995, '84-444-0027-3');
/*!40000 ALTER TABLE `library` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- ******************************* FIN DEL SCRIPT ***************************************

Es importante incluir todas las líneas del script ya que algunas líneas se ejecutan según la versión MySQL

Se ha decidido usar MySQL para persistir los datos denido a la rapidez y versatilidad que aporta para bases de datos del tamaño previsto para una entidad como una biblioteca
El código sigue un patrón MVC y está distribuido en 2 entidades correspondientes al modelo de datos, 2 controladores y 6 vistas.
El controlador llamado MainController hace las veces de iniciador sustituyendo el controlador que genera Symfony por defecto (DefaultController)
El controlador llamado UtilsController gestiona el módulo de listado, de edición, de borrado y de adición de registros (CRUD)

Las vistas se encuentran en una carpeta llamada 'application'
La vista principal (main.html.twig) es la que genera la portada de la aplicación diversificando el acceso al módulo de listado y el de adición de registros
La vista de adicion (add.html.twig) genera un formulario que admite texto y números y muestra si hubo un error al persistir los datos
La vista de lista (list.html.twig) muestra la lista de registros completa en una tabla paginada y con filtros de búsqueda y permite acceder a la edición, vista y borrado de los registros
La vista de detalle (detail.html.twig) muestra información más completa del registro seleccionado y permite editar el registro seleccionado
El borrado se hará tras confirmacion por ventana modal 'confirm' de JavaScript

Autor: Miguel Aguirre Varela