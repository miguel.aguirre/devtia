<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class DefaultControllerTest extends WebTestCase
{
    public function testMain()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
//        $this->assertContains('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }
    
    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/list');
        
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        
        $this->assertContains('Título', $crawler->filter('#table th')->text()); // Test if DataBase contains at least one row
        
//        $this->assertContains('No hay datos para mostrar', $crawler->filter('#noData h2')->text()); // Test if DataBase doesn't contains any row
        
    }
    
    public function testEdit()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/edit/1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Detalles', $crawler->filter('#detailPage')->text());
    }
    
    public function testView()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/view/1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Detalles', $crawler->filter('#detailPage')->text());
    }
    
    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/delete/1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Biblioteca', $crawler->filter('#main div')->text());
    }
    
}
